# Finny Kanban board
Made with Vue, Vuex, Electron, and Firestore.

## Prerequisites
There is an .env.local file with the sensitive connection info to the cloud firestore database.  You will need a copy of this to run locally.

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Run your end-to-end tests
```
npm run test:e2e
```

### Run your unit tests
```
npm run test:unit
```

### Compiles and Hot-reloads Electron app for development
```
npm run electron:serve
```

## Builds Electron app for distribution
```
npm run electron:build
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
