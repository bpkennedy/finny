import { firebase } from './firebase'
import store from './store'

firebase.auth().onAuthStateChanged(async (user) => {
  if (user) {
    try {
      await initializeFirestoreModules(store)
      store.commit(`user/setProfile`, user)
    } catch (error) {
      console.log(error)
    }
  } else {
    await closeFirestoreModules(store)
    store.commit(`user/logout`)
  }
})

async function initializeFirestoreModules (store) {
  await Promise.all([
    store.dispatch('userData/openDBChannel'),
    store.dispatch('boardsData/openDBChannel'),
    store.dispatch('finnysData/openDBChannel'),
    store.dispatch('labelsData/openDBChannel')
  ])
}

async function closeFirestoreModules (store) {
  await Promise.all([
    store.dispatch('userData/closeDBChannel'),
    store.dispatch('boardsData/closeDBChannel'),
    store.dispatch('finnysData/closeDBChannel'),
    store.dispatch('labelsData/closeDBChannel')
  ])
}
