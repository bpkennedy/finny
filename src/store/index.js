import Vue from 'vue'
import Vuex from 'vuex'
import vuexFirestoreModules from './vuexFirestoreModules'
import user from './modules/user'
import boards from './modules/boards'
Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    user,
    boards
  },
  plugins: [vuexFirestoreModules]
})
