const state = {
  boards: [],
  board: {}
}

const getters = {
  boards: (state, getters, rootState) => rootState.boardsData.boards
}

const actions = {
  async getBoards (store) {
    try {
      const querySnapshot = await store.dispatch('boardsData/fetchAndAdd', {}, { root: true })
      if (querySnapshot.done === true) {
        // `{done: true}` is returned when everything is already fetched and there are 0 docs:
        return '0 docs left to retrieve'
      }
    } catch (error) {
      console.log(error)
    }
  },
  async getBoard (store, id) {
    try {
      const querySnapshot = await store.dispatch('boardsData/fetch', { where: [['id', '==', id]] }, { root: true })
      if (querySnapshot.done === true) {
        // `{done: true}` is returned when everything is already fetched and there are 0 docs:
        return '0 docs left to retrieve'
      }
      const fetchedBoard = querySnapshot.docs[0].data()
      fetchedBoard.id = querySnapshot.docs[0].id
      store.commit('boards/setBoard', fetchedBoard, { root: true })
    } catch (error) {
      console.log(error)
    }
  },
  async createBoard (store, { name }) {
    try {
      await store.dispatch('boardsData/set', { name }, { root: true })
    } catch (error) {
      console.log(error)
    }
  },
  async deleteBoard (store, id) {
    try {
      await store.dispatch('boardsData/delete', id, { root: true })
    } catch (error) {
      console.log(error)
    }
  },
  async updateBoard (store, { id, name }) {
    try {
      await store.dispatch('boardsData/patch', { id, name }, { root: true })
    } catch (error) {
      console.log(error)
    }
  }
}

const mutations = {
  setBoard (state, board) {
    state.board = { ...board }
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
