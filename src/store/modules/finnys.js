const state = {
  finnys: [],
  finny: {}
}

const getters = {
  finnys: (state, getters, rootState) => rootState.finnysData.finnys
}

const actions = {
  async getFinnys (store, boardId) {
    try {
      const querySnapshot = await store.dispatch('finnysData/fetchAndAdd', { where: [['board', '==', boardId]] }, { root: true })
      if (querySnapshot.done === true) {
        // `{done: true}` is returned when everything is already fetched and there are 0 docs:
        return '0 docs left to retrieve'
      }
    } catch (error) {
      console.log(error)
    }
  },
  async getFinny (store, finnyId) {
    try {
      const querySnapshot = await store.dispatch('finnysData/fetch', { where: [['id', '==', finnyId]] }, { root: true })
      if (querySnapshot.done === true) {
        // `{done: true}` is returned when everything is already fetched and there are 0 docs:
        return '0 docs left to retrieve'
      }
      const fetchedFinny = querySnapshot.docs[0].data()
      fetchedFinny.id = querySnapshot.docs[0].id
      store.commit('finnys/setFinny', fetchedFinny, { root: true })
    } catch (error) {
      console.log(error)
    }
  },
  async createFinny (store, { name }) {
    try {
      await store.dispatch('finnysData/set', { name }, { root: true })
    } catch (error) {
      console.log(error)
    }
  },
  async deleteFinny (store, id) {
    try {
      await store.dispatch('finnysData/delete', id, { root: true })
    } catch (error) {
      console.log(error)
    }
  },
  async updateFinny (store, { id, name }) {
    try {
      await store.dispatch('finnysData/patch', { id, name }, { root: true })
    } catch (error) {
      console.log(error)
    }
  }
}

const mutations = {
  setFinny (state, finny) {
    state.finny = { ...finny }
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
