import { firebase } from '../../firebase'

const state = {
  profile: {},
  loggedIn: false
}

const getters = {
  profile: ({ profile }) => profile,
  loggedIn: ({ loggedIn }) => loggedIn
}

const actions = {
  async login (store) {
    if (store.state.loggedIn) {
      return
    }
    const provider = new firebase.auth.GoogleAuthProvider()
    try {
      await firebase.auth().signInWithPopup(provider)
    } catch (error) {
      console.log(error)
    }
  },
  async logout () {
    try {
      await firebase.auth().signOut()
    } catch (error) {
      console.log(error)
    }
  }
}

const mutations = {
  setProfile (state, profile) {
    state.loggedIn = true
    state.profile = {
      name: profile.displayName,
      picture: profile.photoURL
    }
  },
  logout (state) {
    state.loggedIn = false
    state.profile = {}
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
