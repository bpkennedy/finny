import createEasyFirestore from 'vuex-easy-firestore'
let vuexFirestoreModules

const userDataModule = {
  firestorePath: 'users/{userId}',
  firestoreRefType: 'doc',
  moduleName: 'userData',
  statePropName: 'user'
}

const boardsDataModule = {
  firestorePath: 'boards',
  firestoreRefType: 'collection',
  moduleName: 'boardsData',
  statePropName: 'boards'
}

const finnysDataModule = {
  firestorePath: 'finnys',
  firestoreRefType: 'collection',
  moduleName: 'finnysData',
  statePropName: 'finnys'
}

const labelsDataModule = {
  firestorePath: 'labels',
  firestoreRefType: 'collection',
  moduleName: 'labelsData',
  statePropName: 'labels'
}

if (process.env.NODE_ENV === 'localhost' || process.env.NODE_ENV === 'development' || process.env.NODE_ENV === 'local') {
  vuexFirestoreModules = createEasyFirestore([
    userDataModule,
    boardsDataModule,
    finnysDataModule,
    labelsDataModule
  ], { logging: true })
} else {
  vuexFirestoreModules = createEasyFirestore([
    userDataModule,
    boardsDataModule,
    finnysDataModule,
    labelsDataModule
  ])
}

export default vuexFirestoreModules
